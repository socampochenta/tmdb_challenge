package test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Presenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.Genre;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.TMDB;

/**
 * Created by Sergio on 8/12/2017.
 */

public class MoviePresenter implements TMDB.Presenter {

    private TMDB.View view;
    private TMDB.Model model;

    public MoviePresenter(TMDB.View view) {
        this.view = view;
        model = new MovieModel(this);
    }

    @Override
    public void showMoviesList(ArrayList<MovieModel> moviesList, String category) {
        if(view!=null){
            view.showMoviesList(moviesList,category);
        }
    }

    @Override
    public void showMoviesListByGenre(HashMap<String, ArrayList<MovieModel>> moviesListGrouped, ArrayList<Genre> genreList) {
        if(view!=null){
            view.showMoviesListByGenre(moviesListGrouped,genreList);
        }
    }

    @Override
    public void showEmptyView(String category,String viewType) {
        if(view!=null){
            view.showEmptyView(category,viewType);
        }
    }

    @Override
    public void getMoviesListByCategory(String category) {
        model.getMoviesListByCategory(category);
    }

    @Override
    public void searchMoviesList(String query) {
        model.searchMoviesList(query);
    }


}
