package test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.Genre;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;

/**
 * Created by Sergio on 8/12/2017.
 */

public interface TMDB {

    interface View{
        void showMoviesList(ArrayList<MovieModel> listMovies, String category);
        void showEmptyView(String category,String typeView);
        void showMoviesListByGenre(HashMap<String,ArrayList<MovieModel>> moviesListGrouped, ArrayList<Genre> genreList);

    }
    interface Presenter{
        void showMoviesList(ArrayList<MovieModel> moviesList, String category);
        void showMoviesListByGenre(HashMap<String,ArrayList<MovieModel>> moviesListGrouped, ArrayList<Genre> genreList);
        void showEmptyView(String category,String typeView);
        void getMoviesListByCategory(String category);
        void searchMoviesList(String query);
    }
    interface Model{
        void getMoviesListByCategory(String category);
        void searchMoviesList(String query);
    }

}
