package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import test.challenge.grabi.tmdb.themoviedatabasechallenge.R;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.Genre;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;

/**
 * Created by Sergio on 11/12/2017.
 */

public class SearchExpandableListView extends BaseExpandableListAdapter {
    private Context _context;
    private ArrayList<Genre> genres; // header titles
    private HashMap<String, ArrayList<MovieModel>> moviesList;

    public SearchExpandableListView(Context context, ArrayList<Genre> genres,
                                 HashMap<String, ArrayList<MovieModel>> moviesList) {
        this._context = context;
        this.genres = genres;
        this.moviesList = moviesList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.moviesList.get(genres.get(groupPosition).getName())
                .get(childPosititon).getTitle();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.search_child_simple_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.text1);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return moviesList.get(genres.get(groupPosition).getName())
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return genres.get(groupPosition).getName();
    }

    @Override
    public int getGroupCount() {
        return this.genres.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.search_group_simple_item, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.text1);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
