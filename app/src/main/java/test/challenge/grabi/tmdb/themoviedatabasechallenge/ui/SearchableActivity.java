package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.App;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.R;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.Genre;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Presenter.MoviePresenter;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.TMDB;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.adapters.SearchExpandableListView;

public class SearchableActivity extends AppCompatActivity implements TMDB.View{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ex_results)
    ExpandableListView ex_results;
    @BindView(R.id.emptyView)
    View wmptyViewWrapper;
    @BindView(R.id.tv_no_data)
    TextView tv_nodata;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.buttonTry)
    Button btnTry;

    TMDB.Presenter mPresenter;
    HashMap<String, ArrayList<MovieModel>> movieListGrouped;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        ButterKnife.bind(this);
        mPresenter = new MoviePresenter(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String query = intent.getStringExtra(SearchManager.QUERY);
            mPresenter.showEmptyView(query,App.TYPE_EMPTY_VIEW_LOADING);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mPresenter.searchMoviesList(query);
                }
            }, 2000);

        }
    }

    @Override
    public void showMoviesList(ArrayList<MovieModel> moviesList, String query) {

    }

    @Override
    public void showEmptyView(String query,String typeView) {
        if(typeView.equals(App.TYPE_EMPTY_VIEW_DONE)){
            wmptyViewWrapper.setVisibility(View.GONE);
        }
        else if(typeView.equals(App.TYPE_EMPTY_VIEW_LOADING)){
            wmptyViewWrapper.setVisibility(View.VISIBLE);
            btnTry.setVisibility(View.GONE);
            tv_nodata.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
        else if(typeView.equals(App.TYPE_EMPTY_VIEW_NO_DATA)){
            wmptyViewWrapper.setVisibility(View.VISIBLE);
            btnTry.setVisibility(View.GONE);
            tv_nodata.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        else if(typeView.equals(App.TYPE_EMPTY_VIEW_ERROR)){
            wmptyViewWrapper.setVisibility(View.VISIBLE);
            btnTry.setVisibility(View.VISIBLE);
            tv_nodata.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMoviesListByGenre(HashMap<String, ArrayList<MovieModel>> moviesListGrouped, ArrayList<Genre> genreList) {
        this.movieListGrouped = moviesListGrouped;

        SearchExpandableListView listAdapter = new SearchExpandableListView(this, genreList, moviesListGrouped);
        ex_results.setAdapter(listAdapter);
    }
}
