package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.App;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.R;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.interfaces.OnItemClickListener;

/**
 * Created by Sergio on 10/12/2017.
 */

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.ViewHolder> {

    private ArrayList<MovieModel> moviesList;
    OnItemClickListener mListener;

    public MoviesListAdapter(ArrayList<MovieModel> moviesList, OnItemClickListener mListener){
        this.moviesList = moviesList;
        this.mListener = mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_movies_list_item, parent, false);

        ViewHolder vh = new ViewHolder(v,mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MovieModel movie = moviesList.get(position);
        holder.tvMovieTitle.setText(movie.getTitle());
        String backdrop = movie.getBackdropPath();
        if(backdrop!=null && !backdrop.isEmpty()){
            Uri uri = Uri.parse(App.BASE_URL_IMAGE + backdrop);
            Glide.with(holder.ivMovieBg).load(uri).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE)).into(holder.ivMovieBg);
        }
    }

    @Override
    public int getItemCount() {
        if(moviesList==null)
            return 0;
        return moviesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_movie_title) TextView tvMovieTitle;
        @BindView(R.id.iv_movie_bg)
        ImageView ivMovieBg;
        OnItemClickListener mListener;

        public ViewHolder(View v, OnItemClickListener mListener) {
            super(v);
            ButterKnife.bind(this,v);
            this.mListener = mListener;
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onItemClick(getAdapterPosition());
        }
    }

    public void setMoviesList(ArrayList<MovieModel> moviesList) {
        this.moviesList = moviesList;
    }
}
