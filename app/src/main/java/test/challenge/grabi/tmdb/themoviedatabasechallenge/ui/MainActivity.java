package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.R;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.Genre;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Presenter.MoviePresenter;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.TMDB;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.fragments.MoviesListFragment;

public class MainActivity extends AppCompatActivity implements TMDB.View, MoviesListFragment.OnFragmentInteractionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    TMDB.Presenter mPresenter;

    ArrayList<String> categories;
    ArrayList<String> categoriesServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter = new MoviePresenter(this);
        initCategoriesData();
        initToolbarAndTabsUI();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    public void initCategoriesData(){
        categories = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.categories)));
        categoriesServices = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.categoriesServices)));
    }

    public void initToolbarAndTabsUI(){
        setSupportActionBar(toolbar);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        int i = 0;
        for (String category:categories){
            Fragment f = MoviesListFragment.newInstance(new ArrayList<MovieModel>(),category,categoriesServices.get(i));
            adapter.addFragment(f);
            i++;
        }
        viewPager.setAdapter(adapter);
    }

    @Override
    public void showMoviesList(ArrayList<MovieModel> listMovies, String categoryService) {
        //show data into fragment
        int indexFragment = getCategoryPos(categoryService);
        if(indexFragment!=-1){
            ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
            MoviesListFragment f = (MoviesListFragment) adapter.getItem(indexFragment);
            f.setMoviesList(listMovies);
        }

    }

    @Override
    public void showEmptyView(String category,String typeView) {
        //show an empty view when not exist data (fragment)
        int indexFragment = getCategoryPos(category);
        if(indexFragment!=-1){
            ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();
            MoviesListFragment f = (MoviesListFragment) adapter.getItem(indexFragment);
            f.showEmptyView(typeView);
        }
    }

    @Override
    public void showMoviesListByGenre(HashMap<String, ArrayList<MovieModel>> moviesListGrouped, ArrayList<Genre> genreList) {

    }


    //Movies List Fragment Listener functions

    @Override
    public void loadDataFromService(String categoryService) {
        if(mPresenter!=null){
            mPresenter.getMoviesListByCategory(categoryService);
        }
    }

    @Override
    public void openDetails(MovieModel movie) {
        Intent i = new Intent(this, MovieDetailActivity.class);
        i.putExtra(MovieDetailActivity.TAG_SEND_MOVIE_DATA, movie);

        startActivity(i);
        overridePendingTransition(R.transition.slide_enter,R.transition.slide_leave);
    }

    private int getCategoryPos(String category) {
        if(categoriesServices==null || categoriesServices.isEmpty())
            return -1;
        return categoriesServices.indexOf(category);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return categories.get(position);
        }
    }
}
