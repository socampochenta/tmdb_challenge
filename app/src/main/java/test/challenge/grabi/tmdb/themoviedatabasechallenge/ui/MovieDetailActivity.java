package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.App;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.R;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;

public class MovieDetailActivity extends AppCompatActivity {


    public static final String TAG_SEND_MOVIE_DATA = "MOVIE_DATA";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_movie_bg)
    ImageView ivMovieBg;
    @BindView(R.id.tv_movie_title)
    TextView tvMovieTitle;
    @BindView(R.id.tv_movie_overview)
    TextView tvMovieOverview;
    @BindView(R.id.tv_movie_popularity)
    TextView tvMoviePopularity;

    MovieModel movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null){
            movie = bundle.getParcelable(TAG_SEND_MOVIE_DATA);
        }

        if(movie!=null){
            Uri uri = Uri.parse(App.BASE_URL_IMAGE+movie.getBackdropPath());
            Glide.with(this).load(uri).apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE)).into(ivMovieBg);
            setTitle(movie.getTitle());
            tvMovieTitle.setText(movie.getTitle());
            tvMovieOverview.setText(new StringBuilder().append("Overview: ").append(movie.getOverview()).toString());
            tvMoviePopularity.setText(new StringBuilder().append("Popularity: ").append(movie.getPopularity()).toString());
        }
        else{
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.transition.slide_leave_right_left, R.transition.slide_enter_right_left);
    }

    @Override public void finish() {
        super.finish();
        overridePendingTransition(R.transition.slide_leave_right_left, R.transition.slide_enter_right_left);
    }

}
