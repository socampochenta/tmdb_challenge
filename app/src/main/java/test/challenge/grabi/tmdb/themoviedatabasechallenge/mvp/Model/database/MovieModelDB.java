package test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.database;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;

/**
 * Created by Sergio on 12/12/2017.
 */

public class MovieModelDB extends SugarRecord<MovieModelDB>{

    private String posterPath;
    private Boolean adult;
    private String overview;
    private String releaseDate;
    private List<Integer> genreIds = new ArrayList<>();
    private Long idMovie;
    private String originalTitle;
    private String originalLanguage;
    private String title;
    private String backdropPath;
    private Double popularity;
    private Integer voteCount;
    private Boolean video;
    private Double voteAverage;
    private String category; //ex. popular top rated...
    private String mainGender;

    public MovieModelDB() {
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }

    public Long getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Long id) {
        this.idMovie = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Boolean getVideo() {
        return video;
    }

    public void setVideo(Boolean video) {
        this.video = video;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getMainGender() {
        return mainGender;
    }

    public void setMainGender(String mainGender) {
        this.mainGender = mainGender;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void saveMoviesListInDB(ArrayList<MovieModel> moviesList){
        List<MovieModelDB> moviesListDBToSave = new ArrayList<>();
        List<MovieModelDB> moviesListDB = getAllMoviesDB();

        if(!moviesListDB.isEmpty()){
            //search the movie and update data
            for (MovieModel movie: moviesList ) {
                boolean finded = false;
                for (MovieModelDB movieDBInDB:moviesListDB) {

                    if(Objects.equals(movieDBInDB.getIdMovie(), movie.getIdMovie())){
                        //update the existing record with the new data
                        setDataMovieDB(movieDBInDB,movie);
                        moviesListDBToSave.add(movieDBInDB);
                        finded = true;
                        break;
                    }

                }
                if(!finded){
                    //new movie database item
                    MovieModelDB movieDB = new MovieModelDB();
                    setDataMovieDB(movieDB,movie);
                    moviesListDBToSave.add(movieDB);
                }
            }

        }
        else{
            //if the database is empty
            for (MovieModel movie: moviesList ) {
                MovieModelDB movieDB = new MovieModelDB();
                setDataMovieDB(movieDB,movie);
                moviesListDBToSave.add(movieDB);
            }
        }
        SugarRecord.saveInTx(moviesListDBToSave);
    }

    public void setDataMovieDB(MovieModelDB movieModelDB, MovieModel movie){
        movieModelDB.setIdMovie(movie.getIdMovie());
        movieModelDB.setAdult(movie.getAdult());
        movieModelDB.setBackdropPath(movie.getBackdropPath());
        movieModelDB.setCategory(movie.getCategory());
        movieModelDB.setGenreIds(movie.getGenreIds());
        movieModelDB.setMainGender(movie.getMainGender());
        movieModelDB.setOriginalLanguage(movie.getOriginalLanguage());
        movieModelDB.setOriginalTitle(movie.getOriginalTitle());
        movieModelDB.setOverview(movie.getOverview());
        movieModelDB.setPopularity(movie.getPopularity());
        movieModelDB.setPosterPath(movie.getPosterPath());
        movieModelDB.setReleaseDate(movie.getReleaseDate());
        movieModelDB.setTitle(movie.getTitle());
        movieModelDB.setVideo(movie.getVideo());
        movieModelDB.setVoteAverage(movie.getVoteAverage());
        movieModelDB.setVoteCount(movie.getVoteCount());
    }

    public ArrayList<MovieModel> getAllMoviesByCategoryFromDB(String category){
        ArrayList<MovieModel> resultMoviesList = new ArrayList<>();
        List<MovieModelDB> moviesListDB = MovieModelDB.find(MovieModelDB.class,"category=?",category);
        if(moviesListDB!=null && !moviesListDB.isEmpty()){
            for (MovieModelDB movieDB:moviesListDB) {
                MovieModel movieModel = new MovieModel();
                movieModel.setIdMovie(movieDB.getIdMovie());
                movieModel.setAdult(movieDB.getAdult());
                movieModel.setBackdropPath(movieDB.getBackdropPath());
                movieModel.setCategory(movieDB.getCategory());
                movieModel.setGenreIds(movieDB.getGenreIds());
                movieModel.setMainGender(movieDB.getMainGender());
                movieModel.setOriginalLanguage(movieDB.getOriginalLanguage());
                movieModel.setOriginalTitle(movieDB.getOriginalTitle());
                movieModel.setOverview(movieDB.getOverview());
                movieModel.setPopularity(movieDB.getPopularity());
                movieModel.setPosterPath(movieDB.getPosterPath());
                movieModel.setReleaseDate(movieDB.getReleaseDate());
                movieModel.setTitle(movieDB.getTitle());
                movieModel.setVideo(movieDB.getVideo());
                movieModel.setVoteAverage(movieDB.getVoteAverage());
                movieModel.setVoteCount(movieDB.getVoteCount());
                resultMoviesList.add(movieModel);
            }

        }
       return resultMoviesList;

    }

    public List<MovieModelDB> getAllMoviesDB(){
        List<MovieModelDB> moviesListDB = MovieModelDB.listAll(MovieModelDB.class);
        if(moviesListDB==null ||moviesListDB.isEmpty()){
            return new ArrayList<>();
        }
       return moviesListDB;
    }

}
