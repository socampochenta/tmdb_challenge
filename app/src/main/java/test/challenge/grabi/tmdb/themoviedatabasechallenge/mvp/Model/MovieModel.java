
package test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.App;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.database.MovieModelDB;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Presenter.MoviePresenter;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.TMDB;

public class MovieModel implements Parcelable, TMDB.Model
{

    @SerializedName("poster_path")
    @Expose
    private String posterPath;
    @SerializedName("adult")
    @Expose
    private Boolean adult;
    @SerializedName("overview")
    @Expose
    private String overview;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("genre_ids")
    @Expose
    private List<Integer> genreIds = new ArrayList<>();
    @SerializedName("id")
    @Expose
    private Long idMovie;
    @SerializedName("original_title")
    @Expose
    private String originalTitle;
    @SerializedName("original_language")
    @Expose
    private String originalLanguage;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("backdrop_path")
    @Expose
    private String backdropPath;
    @SerializedName("popularity")
    @Expose
    private Double popularity;
    @SerializedName("vote_count")
    @Expose
    private Integer voteCount;
    @SerializedName("video")
    @Expose
    private Boolean video;
    @SerializedName("vote_average")
    @Expose
    private Double voteAverage;
    @Expose
    String category; //ex. popular top rated...

    private String mainGender;
    @Ignore
    public TMDB.Presenter presenter;

    public final static Parcelable.Creator<MovieModel> CREATOR = new Creator<MovieModel>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MovieModel createFromParcel(Parcel in) {
            return new MovieModel(in);
        }

        public MovieModel[] newArray(int size) {
            return (new MovieModel[size]);
        }

    }
    ;

    protected MovieModel(Parcel in) {
        this.posterPath = ((String) in.readValue((String.class.getClassLoader())));
        this.adult = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.overview = ((String) in.readValue((String.class.getClassLoader())));
        this.releaseDate = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.genreIds, (java.lang.Integer.class.getClassLoader()));
        this.idMovie = ((Long) in.readValue((Integer.class.getClassLoader())));
        this.originalTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.originalLanguage = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.backdropPath = ((String) in.readValue((String.class.getClassLoader())));
        this.popularity = ((Double) in.readValue((Double.class.getClassLoader())));
        this.voteCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.video = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.voteAverage = ((Double) in.readValue((Double.class.getClassLoader())));
        this.mainGender = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MovieModel() {
    }

    public MovieModel(MoviePresenter presenter){
        this.presenter = presenter;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }

    public Long getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Long idMovie) {
        this.idMovie = idMovie;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Boolean getVideo() {
        return video;
    }

    public void setVideo(Boolean video) {
        this.video = video;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getMainGender() {
        return mainGender;
    }

    public void setMainGender(String mainGender) {
        this.mainGender = mainGender;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(posterPath);
        dest.writeValue(adult);
        dest.writeValue(overview);
        dest.writeValue(releaseDate);
        dest.writeList(genreIds);
        dest.writeValue(idMovie);
        dest.writeValue(originalTitle);
        dest.writeValue(originalLanguage);
        dest.writeValue(title);
        dest.writeValue(backdropPath);
        dest.writeValue(popularity);
        dest.writeValue(voteCount);
        dest.writeValue(video);
        dest.writeValue(voteAverage);
        dest.writeValue(mainGender);
        dest.writeValue(category);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public void getMoviesListByCategory(final String category) {
        //call the api , get the movies list from services
        Call<MovieResultModel> mlc = App.getInstance().getTmdbApi().getMovieByCategory(category);
        mlc.enqueue(new Callback<MovieResultModel>() {

            @Override
            public void onResponse(Call<MovieResultModel> call, Response<MovieResultModel> response) {
                MovieResultModel resultModel = response.body();

                if(resultModel == null || resultModel.getResults()==null || resultModel.getStatus_code()!=null){
                    ArrayList<MovieModel> moviesListFromDb = getMoviesListByCategoryFromDB(category);
                    if(moviesListFromDb.isEmpty()){
                        presenter.showEmptyView(category,App.TYPE_EMPTY_VIEW_NO_DATA);
                    }
                    else{
                        presenter.showEmptyView(category,App.TYPE_EMPTY_VIEW_DONE);
                        presenter.showMoviesList(moviesListFromDb,category);
                    }

                }
                else{
                    for (MovieModel movie: resultModel.getResults()) {
                        movie.setCategory(category);
                    }
                    //save into database
                    saveMoviesListInDB(resultModel.getResults());
                    presenter.showEmptyView(category,App.TYPE_EMPTY_VIEW_DONE);
                    presenter.showMoviesList(resultModel.getResults(),category);
                }

            }

            @Override
            public void onFailure(Call<MovieResultModel> call, Throwable t) {
                ArrayList<MovieModel> moviesListFromDb = getMoviesListByCategoryFromDB(category);
                if(moviesListFromDb.isEmpty()){
                    presenter.showEmptyView(category,App.TYPE_EMPTY_VIEW_ERROR);
                }
                else{
                    presenter.showEmptyView(category,App.TYPE_EMPTY_VIEW_DONE);
                    presenter.showMoviesList(moviesListFromDb,category);
                }
            }
        });
    }

    @Override
    public void searchMoviesList(final String query) {
        //call the api , get the movies list from services
        Call<GenresResultModel> mlcg = App.getInstance().getTmdbApi().getMovieGenres();
        mlcg.enqueue(new Callback<GenresResultModel>() {
            @Override
            public void onResponse(Call<GenresResultModel> call, Response<GenresResultModel> response) {

                GenresResultModel resultGenres = response.body();
                if(resultGenres != null && resultGenres.getGenres()!=null && !resultGenres.getGenres().isEmpty()){
                    final ArrayList<Genre> genres = resultGenres.getGenres();

                    Call<MovieResultModel> mlc = App.getInstance().getTmdbApi().searchMovies(query);
                    mlc.enqueue(new Callback<MovieResultModel>() {

                        @Override
                        public void onResponse(Call<MovieResultModel> call, Response<MovieResultModel> response) {
                            MovieResultModel resultModel = response.body();

                            if(resultModel == null || resultModel.getResults()==null || resultModel.getResults().isEmpty() || resultModel.getStatus_code()!=null ){
                                presenter.showEmptyView(query,App.TYPE_EMPTY_VIEW_NO_DATA);
                            }
                            else{
                                for (MovieModel movie:resultModel.getResults()){
                                    if(movie.getGenreIds()!=null && !movie.getGenreIds().isEmpty()){
                                        int idGenrer = movie.getGenreIds().get(0);
                                        movie.setMainGender(getGenderName(idGenrer,genres));
                                    }
                                    else {
                                        movie.setMainGender("General");
                                    }
                                }
                                //Genero para las peliculas que no tienen genero
                                Genre generalGenre = new Genre();
                                generalGenre.setId(0);
                                generalGenre.setName("General");
                                genres.add(generalGenre);

                                HashMap<String,ArrayList<MovieModel>> moviesGrouped = new HashMap<>();
                                for (Genre genre:genres) {
                                    ArrayList<MovieModel> moviesListFiltered = getMoviesByGenderName(genre.getName(),resultModel.getResults());
                                    if(!moviesListFiltered.isEmpty()){
                                        moviesGrouped.put(genre.getName(),moviesListFiltered);
                                    }
                                }
                                //limpiamos los generos que no tienen resultados
                                ArrayList<Genre> genreListCleaned = cleanGenreList(moviesGrouped,genres);
                                presenter.showEmptyView(query,App.TYPE_EMPTY_VIEW_DONE);
                                presenter.showMoviesListByGenre(moviesGrouped,genreListCleaned);

                            }

                        }

                        @Override
                        public void onFailure(Call<MovieResultModel> call, Throwable t) {
                            presenter.showEmptyView(query,App.TYPE_EMPTY_VIEW_ERROR);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<GenresResultModel> call, Throwable t) {
                presenter.showEmptyView(query,App.TYPE_EMPTY_VIEW_ERROR);
            }
        });


    }

    private ArrayList<Genre> cleanGenreList(HashMap<String, ArrayList<MovieModel>> moviesGrouped, ArrayList<Genre> genres) {
        ArrayList<Genre> genreList = new ArrayList<>();
        for (Genre genre:genres) {
            if(moviesGrouped.containsKey(genre.getName())){
                genreList.add(genre);
            }
        }

        return genreList;
    }

    private String getGenderName(int idGender, List<Genre> genres) {
        String genreName = "General";
        if(genres==null || genres.isEmpty())
            return "General";
        for (Genre genre: genres ) {
            if(genre.getId() == idGender){
                genreName = genre.getName();
                break;
            }
        }
        return genreName;
    }

    private ArrayList<MovieModel> getMoviesByGenderName(String genderName, ArrayList<MovieModel> moviesList) {
        ArrayList<MovieModel> movieListFilteredByGender = new ArrayList<>();

        for (MovieModel movie: moviesList ) {
            if(movie.getMainGender().equalsIgnoreCase(genderName)){
                movieListFilteredByGender.add(movie);
            }
        }
        return movieListFilteredByGender;
    }

    private void saveMoviesListInDB(ArrayList<MovieModel> moviesList){
        MovieModelDB model = new MovieModelDB();
        model.saveMoviesListInDB(moviesList);
    }

    private ArrayList<MovieModel> getMoviesListByCategoryFromDB(String category){
        MovieModelDB model = new MovieModelDB();
        return model.getAllMoviesByCategoryFromDB(category);

    }

}
