package test.challenge.grabi.tmdb.themoviedatabasechallenge.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.GenresResultModel;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieResultModel;

public interface TmdbApi {
    String API_KEY = "a3e37794b6e8fecfa523ce9c1f65e4c2";

    @GET("/3/movie/{category}?api_key="+API_KEY)
    Call<MovieResultModel> getMovieByCategory(@Path("category") String category);
    @GET("/3/search/movie?api_key="+API_KEY)
    Call<MovieResultModel> searchMovies(@Query("query") String query);
    @GET("/3/genre/movie/list?api_key="+API_KEY)
    Call<GenresResultModel> getMovieGenres();
}
