package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.interfaces;

/**
 * Created by Sergio on 10/12/2017.
 */

public interface OnItemClickListener {
    void onItemClick(int position);
}
