package test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.App;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.R;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.MovieModel;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.adapters.MoviesListAdapter;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.ui.interfaces.OnItemClickListener;

public class MoviesListFragment extends Fragment implements OnItemClickListener{

    private static final String ARG_PARAM_LIST_MOVIES = "PARAM_LIST_MOVIES";
    private static final String ARG_PARAM_CATEGORY = "PARAM_CATEGORY";
    private static final String ARG_PARAM_CATEGORY_SERVICE = "PARAM_CATEGORY_SERVICE";

    private ArrayList<MovieModel> moviesList;
    private String category;
    private String categoryService;

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.recycler_view_movies_list)
    RecyclerView recyclerViewMovieList;
    @BindView(R.id.emptyView)
    View wmptyViewWrapper;
    @BindView(R.id.tv_no_data)
    TextView tv_nodata;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.buttonTry)
    Button btnTry;
    MoviesListAdapter adapter;

    public MoviesListFragment() {
        // Required empty public constructor
    }

    public static MoviesListFragment newInstance(ArrayList<MovieModel> moviesList, String category, String categoryService) {
        MoviesListFragment fragment = new MoviesListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM_LIST_MOVIES,moviesList);
        args.putString(ARG_PARAM_CATEGORY, category);
        args.putString(ARG_PARAM_CATEGORY_SERVICE,categoryService);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            moviesList = savedInstanceState.getParcelableArrayList(ARG_PARAM_LIST_MOVIES);
            category = savedInstanceState.getString(ARG_PARAM_CATEGORY);
            categoryService = savedInstanceState.getString(ARG_PARAM_CATEGORY_SERVICE);
        }
        else if (getArguments() != null) {
            moviesList = getArguments().getParcelableArrayList(ARG_PARAM_LIST_MOVIES);
            category = getArguments().getString(ARG_PARAM_CATEGORY);
            categoryService = getArguments().getString(ARG_PARAM_CATEGORY_SERVICE);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(ARG_PARAM_LIST_MOVIES,moviesList);
        outState.putString(ARG_PARAM_CATEGORY,category);
        outState.putString(ARG_PARAM_CATEGORY_SERVICE,categoryService);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies_list, container, false);
        ButterKnife.bind(this,view);
        initMoviesListView();
        checkAndLoadData();
        btnTry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAndLoadData();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void initMoviesListView(){
        recyclerViewMovieList.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewMovieList.setLayoutManager(mLayoutManager);
        adapter = new MoviesListAdapter(moviesList,this);
        recyclerViewMovieList.setAdapter(adapter);
    }

    public void checkAndLoadData(){

        if(moviesList==null || moviesList.isEmpty()){
            if(mListener!=null){
                showEmptyView(App.TYPE_EMPTY_VIEW_LOADING);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mListener.loadDataFromService(categoryService);
                    }
                }, 2000);
            }
        }
    }

    public List<MovieModel> getMoviesList() {
        return moviesList;
    }

    public void setMoviesList(ArrayList<MovieModel> moviesList) {
        this.moviesList = moviesList;
        adapter.setMoviesList(moviesList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        mListener.openDetails(moviesList.get(position));
    }

    public void showEmptyView(String typeView) {
        if(typeView.equals(App.TYPE_EMPTY_VIEW_DONE)){
            wmptyViewWrapper.setVisibility(View.GONE);
        }
        else if(typeView.equals(App.TYPE_EMPTY_VIEW_LOADING)){
            wmptyViewWrapper.setVisibility(View.VISIBLE);
            btnTry.setVisibility(View.GONE);
            tv_nodata.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
        else if(typeView.equals(App.TYPE_EMPTY_VIEW_NO_DATA)){
            wmptyViewWrapper.setVisibility(View.VISIBLE);
            btnTry.setVisibility(View.GONE);
            tv_nodata.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        else if(typeView.equals(App.TYPE_EMPTY_VIEW_ERROR)){
            wmptyViewWrapper.setVisibility(View.VISIBLE);
            btnTry.setVisibility(View.VISIBLE);
            tv_nodata.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void loadDataFromService(String categoryService);
        void openDetails(MovieModel movie);
    }
}
