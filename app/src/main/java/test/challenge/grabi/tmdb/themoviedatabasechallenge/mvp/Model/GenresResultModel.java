
package test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GenresResultModel implements Parcelable
{

    @SerializedName("genres")
    @Expose
    private ArrayList<Genre> genres = new ArrayList<>();
    public final static Parcelable.Creator<GenresResultModel> CREATOR = new Creator<GenresResultModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GenresResultModel createFromParcel(Parcel in) {
            return new GenresResultModel(in);
        }

        public GenresResultModel[] newArray(int size) {
            return (new GenresResultModel[size]);
        }

    }
            ;

    protected GenresResultModel(Parcel in) {
        in.readList(this.genres, (test.challenge.grabi.tmdb.themoviedatabasechallenge.mvp.Model.Genre.class.getClassLoader()));
    }

    public GenresResultModel() {
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(genres);
    }

    public int describeContents() {
        return 0;
    }

}