package test.challenge.grabi.tmdb.themoviedatabasechallenge;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.SugarApp;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.challenge.grabi.tmdb.themoviedatabasechallenge.api.TmdbApi;

public class App extends SugarApp {

    //empty view tags
    public static final String TYPE_EMPTY_VIEW_LOADING = "show_loading";
    public static final String TYPE_EMPTY_VIEW_NO_DATA = "show_message_empty_data";
    public static final String TYPE_EMPTY_VIEW_ERROR = "show_error";
    public static final String TYPE_EMPTY_VIEW_DONE = "hide_empty_view";

    //Retrofit - Configuration
    private static final String BASE_URL = "https://api.themoviedb.org/";
    public static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500";

    Retrofit retrofit;
    // Service
    TmdbApi tmdbApi;

    // Create the instance
    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initRetrofit();
        initApi();
    }

    public static App getInstance(){
        return instance;
    }

    public void initApi(){
        if(retrofit == null)
            initRetrofit();
        tmdbApi = retrofit.create(TmdbApi.class);
    }

    public void initRetrofit(){
        //Configuramos Retrofit para que funcione con Gson
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }


    public Retrofit getRetrofit() {
        if(retrofit==null)
            initRetrofit();
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public TmdbApi getTmdbApi() {
        if(tmdbApi == null)
            initApi();
        return tmdbApi;
    }

    public void setTmdbApi(TmdbApi tmdbApi) {
        this.tmdbApi = tmdbApi;
    }

}
